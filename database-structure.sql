-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2019 at 08:26 AM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 7.0.30-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sunnyside-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `change_log`
--

CREATE TABLE IF NOT EXISTS `change_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(45) NOT NULL,
  `new_data` varchar(80) NOT NULL,
  `old_data` varchar(80) NOT NULL,
  `date_of_change` datetime NOT NULL,
  `affected_table` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_change_log_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `deduction`
--

CREATE TABLE IF NOT EXISTS `deduction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `single_percentage` double NOT NULL,
  `shared_percentage` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identification` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(80) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `start_day` date NOT NULL,
  `position_name` varchar(80) DEFAULT NULL,
  `hours_per_week` double DEFAULT NULL,
  `position_id` int(11) NOT NULL,
  `working_day_id` int(11) NOT NULL,
  `pay_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `indetification_UNIQUE` (`identification`),
  KEY `fk_employee_puesto1_idx` (`position_id`),
  KEY `fk_employee_jornada1_idx` (`working_day_id`),
  KEY `fk_employee_tipo_de_pago1_idx` (`pay_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_deduction`
--

CREATE TABLE IF NOT EXISTS `employee_deduction` (
  `employee_id` int(11) NOT NULL,
  `deduction_id` int(11) NOT NULL,
  PRIMARY KEY (`employee_id`,`deduction_id`),
  KEY `fk_employee_deduction_deduccion1_idx` (`deduction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `historical_deduction`
--

CREATE TABLE IF NOT EXISTS `historical_deduction` (
  `proof_of_payment_id` int(11) NOT NULL,
  `percentaje` double NOT NULL,
  `name` varchar(60) NOT NULL,
  `total` double NOT NULL,
  KEY `fk_historical_deduction_proof_of_payment1_idx` (`proof_of_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `increments`
--

CREATE TABLE IF NOT EXISTS `increments` (
  `proof_of_payment_id` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `name` varchar(45) NOT NULL,
  `value` double NOT NULL,
  KEY `fk_increments_proof_of_payment1_idx` (`proof_of_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pay_roll`
--

CREATE TABLE IF NOT EXISTS `pay_roll` (
  `ordinary_salary` double NOT NULL,
  `extra_hour` double DEFAULT NULL,
  `double_hour` double DEFAULT NULL,
  `position_id` int(11) NOT NULL,
  `pay_type_id` int(11) NOT NULL,
  `working_day_id` int(11) NOT NULL,
  `id` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`position_id`,`pay_type_id`,`working_day_id`),
  KEY `fk_planilla_puesto1_idx` (`position_id`),
  KEY `fk_planilla_jornada1_idx` (`working_day_id`),
  KEY `fk_planilla_tipo_de_pago1_idx` (`pay_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pay_type`
--

CREATE TABLE IF NOT EXISTS `pay_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `proof_of_payment`
--

CREATE TABLE IF NOT EXISTS `proof_of_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `responsable_user_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `beagin_of_period` date NOT NULL,
  `end_of_period` date NOT NULL,
  `payday` datetime NOT NULL,
  `ordinary_salary` double NOT NULL,
  `position` varchar(60) NOT NULL,
  `working_day` varchar(45) NOT NULL,
  `hours_per_week` double DEFAULT NULL,
  `pay_type` varchar(45) NOT NULL,
  `gross_salary` double NOT NULL,
  `net_salary` double NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proof_of_payment_employee1_idx` (`employee_id`),
  KEY `fk_proof_of_payment_user1_idx` (`responsable_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `roll`
--

CREATE TABLE IF NOT EXISTS `roll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `roll_has_permissions`
--

CREATE TABLE IF NOT EXISTS `roll_has_permissions` (
  `roll_id` int(11) NOT NULL,
  `permissions_id` int(11) NOT NULL,
  PRIMARY KEY (`roll_id`,`permissions_id`),
  KEY `fk_roll_has_permissions_permissions1_idx` (`permissions_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `roll_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_user_roll1_idx` (`roll_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `working_day`
--

CREATE TABLE IF NOT EXISTS `working_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `change_log`
--
ALTER TABLE `change_log`
  ADD CONSTRAINT `fk_change_log_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `fk_employee_jornada1` FOREIGN KEY (`working_day_id`) REFERENCES `working_day` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_puesto1` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_tipo_de_pago1` FOREIGN KEY (`pay_type_id`) REFERENCES `pay_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee_deduction`
--
ALTER TABLE `employee_deduction`
  ADD CONSTRAINT `fk_employee_deduction_deduccion1` FOREIGN KEY (`deduction_id`) REFERENCES `deduction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_deduction_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `historical_deduction`
--
ALTER TABLE `historical_deduction`
  ADD CONSTRAINT `fk_historical_deduction_proof_of_payment1` FOREIGN KEY (`proof_of_payment_id`) REFERENCES `proof_of_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `increments`
--
ALTER TABLE `increments`
  ADD CONSTRAINT `fk_increments_proof_of_payment1` FOREIGN KEY (`proof_of_payment_id`) REFERENCES `proof_of_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pay_roll`
--
ALTER TABLE `pay_roll`
  ADD CONSTRAINT `fk_planilla_jornada1` FOREIGN KEY (`working_day_id`) REFERENCES `working_day` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_planilla_puesto1` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_planilla_tipo_de_pago1` FOREIGN KEY (`pay_type_id`) REFERENCES `pay_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `proof_of_payment`
--
ALTER TABLE `proof_of_payment`
  ADD CONSTRAINT `fk_proof_of_payment_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proof_of_payment_user1` FOREIGN KEY (`responsable_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `roll_has_permissions`
--
ALTER TABLE `roll_has_permissions`
  ADD CONSTRAINT `fk_roll_has_permissions_permissions1` FOREIGN KEY (`permissions_id`) REFERENCES `permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_roll_has_permissions_roll1` FOREIGN KEY (`roll_id`) REFERENCES `roll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_roll1` FOREIGN KEY (`roll_id`) REFERENCES `roll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
