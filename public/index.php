<?php
  header('Content-type: text/html; charset=UTF-8');
  session_start();

  $base = dirname( __DIR__	). '/';
  
  if(file_exists($base.'.env')){
    foreach(file($base.'.env') as $line){
      $e = explode('=', $line);
      if(isset($e[0]) && $e[0] != '' && isset($e[1]) && $e[1] != '' ){
        $_ENV[trim($e[0])] = trim($e[1]);
      }
    }
  }
  $_ENV['base'] = $base;
  
  date_default_timezone_set($_ENV['TIMEZONE']);
  setlocale(LC_MONETARY, $_ENV['MONEY']);

  require $base . 'Routing/Core.php';

  $routingCore =  new Core();
  $error404Page = "404page";
  $mainFolderPath = $base . "src/controllers/";

  $routingCore->main($error404Page, $mainFolderPath);
?>
