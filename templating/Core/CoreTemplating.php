<?php
  global $base;
  require 'RenderEngine.php';

  class CoreTemplating{
      protected $cachePath;
      protected $templatePath;
      private $engine;
      private $cacheOn;
      private $templateExtention;

      public function __construct(){
          global $base;
          $this->cachePath = $base . 'templating/Core/cache/';
          $this->templatePath = $base . 'templating/';
          $this->cacheOn = ($_ENV['CACHE_ON'] === 'true');
          $this->templateExtention = $_ENV['TEMPLATE_EXTENSION'];

          $this->engine = new RenderEngine($this->cachePath, $this->templatePath, $this->templateExtention);
      }

      public function render($view, $params){
        $renderedTemplate = $this->cachePath . $view . '.php';
        $template = $this->templatePath . $view . $this->templateExtention;

        if(file_exists($template) && file_exists($renderedTemplate)){
          $tempMoreNewThatRender = filemtime($template) - filemtime($renderedTemplate);
        }else{
          $tempMoreNewThatRender = 1;
        }

        if(!($this->cacheOn && file_exists($renderedTemplate) && ($tempMoreNewThatRender <= 0)) || (time() - filemtime($renderedTemplate) > 600)){
          if(file_exists($template)){
            $this->engine->run($view);
          }else{
            var_dump("Not exits the template");
          }
        }


        if(file_exists($renderedTemplate)){
          foreach ($params as $key => $param) {
            $$key = $param;
          }

          require $renderedTemplate;
        }else{
          var_dump("Something happen. The rendered template doesn't exist.");
        }

      }
  }
?>
