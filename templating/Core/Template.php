<?php
  class Template{
    private $file;
    private $filePath;
    private $inheritData;
    private $viewToInheritData;
    private $sectionsData;
    private $templatePath;
    private $templateExtention;

    public function __construct($filePath, $templatePath = '', $templateExtention = ''){
      $this->filePath = $filePath;
      $this->templatePath = $templatePath;
      $this->templateExtention = $templateExtention;
      $this->file = array_map('trim', array_filter(file($filePath, FILE_IGNORE_NEW_LINES), array($this, 'nonempty')));
      $this->file = $this->deleteComments($this->file);

    }

    public function getPath(){
      return $this->filePath;
    }

    public function viewToInherit(){
      if(!isset($this->viewToInheritData)){
        $sayExtends = array_filter($this->file, array($this, 'lineHasExtends'));

        switch (count($sayExtends)){
          case 1:
            $this->viewToInheritData = $this->clearStandarParams($sayExtends[0])[0];
              break;
          default:
            var_dump("Multiple inheritance is not allowed. Template: " . $this->filePath);
          case 0:
            $this->viewToInheritData = '';
        }
      }
      return $this->viewToInheritData;
    }

    /**
    * @return boolean: If this template inherit
    **/
    public function inherit(){
      if(!isset($this->inheritData)){
        $this->inherit = ($this->viewToInherit() != '');
      }
      return $this->inherit;
    }

    public function sections(){
      if(!isset($this->sectionsData)){
        $sections = new class{};
        $openSection = null;
        $multipleLineSection = [];
        foreach($this->file as $line){

          if(strpos($line, '@endsection') !== false || strpos($line, '@stop') !== false){
            $sections->$openSection = $multipleLineSection;
            $openSection = null;
            $multipleLineSection = [];
          }elseif(isset($openSection)){
            array_push($multipleLineSection, $line);
          }elseif(strpos($line, '@section') !== false){
            $params = $this->clearStandarParams($line);

            $name = $params[0];

            if(count($params) == 2){
              $sections->$name = $params[1];
            }else{
              $openSection = $name;
            }
          }
        }
        $this->sectionsData = $sections;
      }
      return $this->sectionsData;
    }

    public function setChildSections($childSections){
      $this->file = array_values($this->file);

      $sectionIsOpen = false;
      $parentSectionContent = [];
      foreach($this->file as $i => &$line){
      // for($i = 0; $i < count($this->file); $i++){
        if(strpos($line, '@yield') !== false){
          $name = $this->clearStandarParams($line)[0];

          if(isset($childSections->$name) && gettype($childSections->$name) == 'array'){
            $line = $childSections->$name;
          }elseif(isset($childSections->$name)){
            $line = preg_replace("/@yield\([\S]*\)/", $childSections->$name, $line);
          }else{
            $line = preg_replace("/@yield\([\S]*\)/", '', $line);
          }
        }

        if((gettype($line) == 'string') && strpos($line, '@show') !== false){
          //$line = preg_replace("/@parent/", $parentSectionContent, $childSections->$name);
          if(isset($childSections->$name) && gettype($childSections->$name) == 'string'){
            $line = preg_replace("/@parent/", $parentSectionContent, $childSections->$name);
          }elseif(isset($childSections->$name) && gettype($childSections->$name) == 'array'){
            foreach($childSections->$name as &$subL){
              if(strpos($subL, '@parent') !== false){
                $subL = $parentSectionContent;
              }
            }
            $line = $childSections->$name;
          }else{
            $line = '';
          }
          $parentSectionContent = [];
          $sectionIsOpen = false;
        }elseif($sectionIsOpen){
          // $parentSectionContent .= $line;
          array_push($parentSectionContent, $line);
          $line = '';
        }elseif((gettype($line) == 'string') && strpos($line, '@section') !== false){
          $sectionIsOpen = true;
          $name = $this->clearStandarParams($line)[0];
          $line = '';
        }
      }

      $this->file = array_filter($this->unNestedArray($this->file), array($this, 'nonempty'));
    }

    public function compile(){
      global $base;
      $rendered = '';
      foreach ($this->file as $i => $line) {
        if($line != ''){
          if($line{0} == '@'){
            $name = trim($this->getName($line));
            $params = trim($this->getParams($line));

            switch ($name) {
              case 'section':
                $p = array_map('trim', explode(',', str_replace('\'', '', $params)));
                $name = $p[0];
                var_dump($p);
                if(count($p) == 1){
                  $sectionOpen = new class{};
                  $sectionOpen->name = $name;
                }elseif(count($p) == 2){
                  $child->section->$name = $p[1];
                  var_dump($child);
                }
                break;
              case 'include':
                $path = $this->templatePath . $this->clearStandarParams($line)[0] . $this->templateExtention;
                $compoment = new Template($path);
                $line = $compoment->compile();
                break;
              case 'if':
                $line = str_replace('@', '<?php ', $line) . '{?>';
                break;
              case 'elseif':
              case 'else':
                $line = str_replace('@', '<?php }', $line) . '{?>';
                break;
              case 'unless':
                $line = str_replace('@unless', '<?php if ', $line);
                $line = preg_replace('/\(/', '(!(', $line, 1) . '){?>';
                // $line =
                break;
              case 'isset':
                $line = str_replace('@isset', '<?php if ', $line);
                $line = preg_replace('/\(/', '(isset(', $line, 1) . '){?>';
                break;
              case 'for':
                $line = str_replace('@for', '<?php for ', $line) . '{?>';
                break;
              case 'foreach':
                $line = str_replace('@foreach', '<?php foreach ', $line) . '{?>';
                break;
              case 'while':
                $line = str_replace('@while', '<?php while ', $line) . '{?>';
                break;
              case 'break':
                if($params == ''){
                  $line = '<?php break; ?>';
                }else{
                  $line = str_replace('@break', '<?php if ', $line) . '{break;}?>';
                }
                break;
              case 'continue':
                if($params == ''){
                  $line = '<?php continue; ?>';
                }else{
                  $line = str_replace('@continue', '<?php if ', $line) . '{continue;}?>';
                }
                break;
              case 'php':
                $line = '<?php ';
                break;
              case 'endphp':
                $line = '?>';
                break;
              case 'endif':
              case 'endunless':
              case 'endisset':
              case 'endempty':
              case 'endfor':
              case 'endforeach':
              case 'endwhile':
                $line = '<?php } ?>';
                break;
            }
          }
          elseif (strpos($line,'<link') === 0) {
            if(strpos($line, 'compile') > 0){
              $parts = explode(' ', $line);
              foreach ($parts as $part) {
                if(strpos($part,'href="') === 0){
                  $a = explode('"', $part)[1];
                  $route = preg_replace("/\.\//", $base, $a);
                  $css = ($_ENV['MINIFY_CSS'] == 'false')?file_get_contents($route):$this->minifyCSS(file($route));
                  $rendered .= '<style>' . $css . '</style>';
                }
              }
              $line = '';
            }
          }

          if(preg_match("/{{[\s\S]*}}/", $line)){
            $line = str_replace('{{', '<?= ', $line);
            $line = str_replace('}}', ' ?>', $line);
          }

          $rendered .= $line;
        }
      }
      return $rendered;
    }

    private function lineHasExtends($line){
      return strpos($line, '@extends') !== false;
    }

    private function nonempty($element){
      return trim($element) != '';
    }

    private function clearStandarParams($line){
      $init = strpos($line, '(')+1;
      $end = strrpos($line, ')');
      $length = $end - $init;

      $paramsText = substr($line, $init, $length);

      $params = explode(',', $paramsText);
      $params = array_map('trim', $params);
      $params = array_map(array($this, 'clearCapsule'), $params);
      return $params;
    }

    private function clearCapsule($text){
      return substr($text, 1, strlen($text)-2);
    }

    private function deleteComments ($list){
      $commentOpenOn = -1;
      foreach($list as $i => &$line){
        $resultLine = $line;
        $init = strpos($line, '{{--');
        $end = strrpos($line, '--}}')+4;

        if($commentOpenOn > -1){
          $resultLine = '';
        }

        if($init !== false){
          $commentOpenOn = $i;
          $resultLine = substr($line, 0, $init);
        }

        if($end !== false && $end > 4 && $commentOpenOn > -1){
          if($commentOpenOn == $i){
            $resultLine .= substr($line, $end, strlen($line));
          }else{
            $resultLine = substr($line, $end, strlen($line));
          }
          $commentOpenOn = -1;
        }

        $line = $resultLine;
        if($line == ''){
          unset($list[$i]);
        }
      }
      return $list;
    }

    private function unNestedArray($array){
      $result = [];
      return $this->deepUnNestedArray($array, $result);
    }

    private function deepUnNestedArray($array, &$result){
      foreach($array as $el){
        if(gettype($el) == 'string'){
          array_push($result, $el);
        }elseif(gettype($el) == 'array'){
          $this->deepUnNestedArray($el, $result);
        }
      }
      return $result;
    }

    private function getName($line){
      $init = strpos($line, '@')+1;
      $end = strpos($line, '(');
      if($end === false){
        $end = strlen($line);
      }else{
        $end--;
      }

      return substr($line, $init, $end);
    }

    private function getParams($line){
      $name = $this->getName($line);
      return substr($line, 2 + strlen($name), strlen($line) - strlen($name) - 3);
    }
    
    private function minifyCSS($list){
      $result = '';
      
      $commentIsOpen = false;
      foreach($list as $line){
        $line = trim($line);
        if($line != ''){
          if(strpos($line, '/*') !== false){
            $commentIsOpen = true;
            $result .= substr($line, 0, strpos($line, '/*'));
          }
          if(!$commentIsOpen){
            $result .= $line;
          }
          if(strpos($line, '*/') !== false){
            $commentIsOpen = false;
            $result .= substr($line, strpos($line, '*/')+2, strlen($line));
          }
        }
      } 
      return $result;
    }
  }
?>
