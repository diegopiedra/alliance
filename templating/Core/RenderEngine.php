<?php
  require 'Template.php';
  class RenderEngine{
    private $cachePath;
    private $templatePath;
    private $templateExtention;

    public function __construct($cachePath, $templatePath, $templateExtention){
      $this->cachePath = $cachePath;
      $this->templatePath = $templatePath;
      $this->templateExtention = $templateExtention;
    }

    public function run($view){
      $filePath = $this->templatePath . $view . $this->templateExtention;

      $template = new Template($filePath, $this->templatePath, $this->templateExtention);
      if($template->inherit()){
        $parentPath = $this->templatePath . $template->viewToInherit() . $this->templateExtention;

        $rendered = '';
        if(file_exists($parentPath)){
          $parentTemplate = new Template($parentPath, $this->templatePath, $this->templateExtention);
          $childSections = $template->sections();
          $parentTemplate->setChildSections($childSections);

          $rendered = $parentTemplate->compile();
        }else{
          var_dump("Don't exist parent file $parentPath for " . $filePath);
        }
      }else{
        $rendered = $template->compile();
      }
      $this->createFile($rendered, $view);
    }

    private function createFile($content, $view){
      file_put_contents($this->cachePath . $view . '.php', $content);
    }
  }
?>
