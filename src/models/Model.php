<?php
  /**
   *
   */
  class Model{
    private $db;
    private $child;
    private $uniqueIdentifierName;
    private $changedProperties;

    public function __construct($uniqueIdentifierName = 'id'){
      
      $connection = $_ENV['DB_CONNECTION']; 
      $host = $_ENV['DB_HOST']; 
      $port = $_ENV['DB_PORT']; 
      $database = $_ENV['DB_DATABASE']; 
      $user = $_ENV['DB_USERNAME']; 
      $password = $_ENV['DB_PASSWORD']; 
      
      try{
        $this->db = new PDO("$connection:host=$host;port=$port;dbname=$database", $user, $password);
        $this->db->exec("SET CHARACTER SET utf8");
       } catch(PDOException $ex) {
        echo "No se puede conectar a la Base datos.<br>Mensaje tecnico: "; //user friendly message
        echo $ex->getMessage();
        // some_logging_function($ex->getMessage());
      }

      $this->child = get_called_class();
      $this->uniqueIdentifierName = $uniqueIdentifierName;
      $this->changedProperties = [];
    }

    protected function sql($sql){
      return $this;
    }

    public function load($many = false){
      $params = array_filter(get_object_vars($this), function ($value, $name){
        return property_exists($this->child, $name) && $value != null;
      }, ARRAY_FILTER_USE_BOTH);

      // var_dump($params);

      $selectQuery = $this->createSQLSelectPDO($params, $many);

      // var_dump($selectQuery);

      $ok = $this->executePDO($selectQuery, $params, $many);

      $this->changedProperties = [];

      return $ok;
    }

    public function save(){
      if(count($this->changedProperties) > 0){
        $id = $this->uniqueIdentifierName;
        if(isset($this->$id)){
          $this->update();
        }else{
          $this->insert();
        }
        $this->changedProperties = [];
      }
    }

    public function delete(){
      $id = $this->uniqueIdentifierName;
      if(isset($this->$id)){
        $deleteQuery = $this->createSQLDeletePDO();
        // var_dump($deleteQuery);

        $sentencia = $this->db->prepare($deleteQuery);
        $id = $this->g;
        $sentencia->bindParam(':'.$id, $this->$id);

        return $sentencia->execute();
      }
    }

    private function createSQLDeletePDO(){
      $sql = "DELETE FROM " . $this->getTableName($this->child) . " WHERE " . $this->uniqueIdentifierName . " = :" . $this->uniqueIdentifierName;
      return $sql;
    }

    private function update(){
      $updateQuery = $this->createSQLUpdatePDO($this->changedProperties);
      // dump($updateQuery);

      $sentencia = $this->db->prepare($updateQuery);

      foreach($this->changedProperties as $name){
        $sentencia->bindParam(':'.$name, $this->$name);
      }


      $id = $this->uniqueIdentifierName;
      $sentencia->bindParam(':'.$id, $this->$id);

      $sentencia->execute();
    }

    private function createSQLUpdatePDO($params){
      $sql = "UPDATE " . strtolower($this->child) . " SET ";

      foreach($params as $value){
        $sql .= $value . " = :" . $value;

        $lastItem = end($params);
        if($lastItem !== $value){
          $sql .= ', ';
        }else{
          $sql .= ' ';
        }
      }
      $id = $this->uniqueIdentifierName;
      $sql .= "WHERE " . $id  . " = :" . $id;

      return $sql;
    }

    private function insert(){
      $insertQuery = $this->createSQLInsertPDO($this->changedProperties);
      // var_dump($insertQuery);

      $sentencia = $this->db->prepare($insertQuery);

      foreach($this->changedProperties as $name){
        $sentencia->bindParam(':'.$name, $this->$name);
      }

      $sentencia->execute();

      $sentencia->setFetchMode(PDO::FETCH_CLASS, $this->child);

      $id = $this->uniqueIdentifierName;
      $this->$id = $this->db->lastInsertId();
    }

    private function createSQLInsertPDO($params){
      $sql = "INSERT INTO " . $this->getTableName($this->child) . " (";

      foreach($params as $value){
        $sql .= $value;

        $lastItem = end($params);
        if($lastItem !== $value){
          $sql .= ', ';
        }
      }

      $sql .= ") VALUES (";

      foreach($params as $value){
        $sql .= ":" . $value;

        $lastItem = end($params);
        if($lastItem !== $value){
          $sql .= ', ';
        }
      }

      $sql .= ");";

      return $sql;
    }

    private function executePDO($query, $params, $many = false){
      try {
        $sentence = $this->db->prepare($query);
        foreach($params as $param => $val){
          $sentence->bindValue(':' . $param, $val);
        }

        $sentence->execute();

        if(!$many){
          $sentence->setFetchMode(PDO::FETCH_INTO , $this);

          $res = $sentence->fetch();
          return ($res !== false);
        }else{
          $class = get_called_class();
          $sentence->setFetchMode(PDO::FETCH_CLASS , $class);

          $res = $sentence->fetchAll();
          return $res;
        }
        
        
        // var_dump($sentence->errorInfo());

        

      } catch(PDOException $ex) {
        echo "An Error occured!"; //user friendly message
        some_logging_function($ex->getMessage());
      }
    }

    private function createSQLSelectPDO($params, $many = false){
      // var_dump($this->getChildProperties());
      
      if($many){
        $needData = $this->getChildProperties();
      }else{
        $needData = array_diff_assoc($this->getChildProperties(), $params);
      }

      end($needData);
      $lastItem = key($needData);

      $sql = "SELECT ";
      foreach($needData as $name => $value){
        $sql .= $name;
        if ($name === $lastItem) {
            $sql .= ' ';
        }else{
          $sql .= ', ';
        }
      }

      $sql .= "FROM " . $this->getTableName($this->child);

      if(count($params) > 0){
        $sql .= " WHERE ";

        end($params);
        $lastItem = key($params);

        foreach ($params as $name => $value) {
          $sql .= $name . ' = :' . $name;

          if($name !== $lastItem){
            $sql .= ' and ';
          }
        }

        if(!$many){
          $sql .= ' LIMIT 1;';
        }else{
          $sql .= ';';
        }
        
      }

      return $sql;
    }

    private function getChildProperties(){
      return array_filter(get_object_vars($this), function ($name){
        return property_exists($this->child, $name);
      }, ARRAY_FILTER_USE_KEY);
    }

    public function __set($name, $value){
      if(property_exists($this->child, $name)){
        $this->$name = $value;
        $this->registerChange($name);
      }
    }

    public function __get($name){
      if(property_exists($this->child, $name)){
        return $this->$name;
      }
    }

    public function __toString(){
      $response = '{';
      $list = $this->getChildProperties();
      foreach($list as $name => $value){
        $response .= "\"$name\": \"$value\"";
        end($list);
        $lastItem = key($list);

        if($name !== $lastItem){
          $response .= ', ';
        }
      }
      return $response . '}';
    }

    private function registerChange($name){
      if(!in_array($name, $this->changedProperties)){
        array_push($this->changedProperties, $name);
      }
    }

    public static function all(){
      $class = get_called_class();
      
      $obj = new $class();
      $sql = "SELECT * FROM " . $obj->getTableName($class) . ";";

      
      $sentence = $obj->db->prepare($sql);

      $sentence->execute();

      $sentence->setFetchMode(PDO::FETCH_CLASS, $class);

      return $sentence->fetchAll();
    }

    public static function find($propertyValue, $propertyName = null, $many = false){
      $class = get_called_class();
      $obj = new $class();
      
      if($propertyName == null){
        $idName = $obj->uniqueIdentifierName;
        $obj->$idName = $propertyValue;
      }else{
        $obj->$propertyName = $propertyValue;
      }
      
      $load = $obj->load($many);
      if($load && !$many){
        return $obj;
      }elseif($load && $many){
        return $load;
      }elseif(!$load && $many){
        return [];
      }
      return null;
    }

    public static function destroy(...$ids){
      $class = get_called_class();
      $list = [];
      foreach($ids as $id){
        $obj = new $class();
        $idName = $obj->uniqueIdentifierName;
        $obj->$idName = $id;
        if($obj->load()){
          if($obj->delete()){
            array_push($list, $id);
          }
        }
      }
      return $list;
    }

    public function where($name, $value){
      $class = get_called_class();
      $obj = (isset($this))?$this: new $class();
      $obj->$name = $value;
      return $obj;
    }

    public function get(){
      $this->load();
      return $this;
    }
    
    /*
    * @param String $index page to show
    * @param String $length quantity of items per page
    
    * @return Array
    */
    public static function paginate($index = 1, $length = 10, $order = 'ASC', $orderBy = null, $search = null){
      $response = [];
      $class = get_called_class();
      $obj = new $class();
      $numberOfTuples = $class::length();
      
      $response['end'] = $index * $length;
      $response['init'] = $response['end'] - $length + 1;
      $response['total'] = $numberOfTuples;
      $response['lines'] = $length;
      $response['page'] = $index;
      
      
      $sql = $obj->createSQLSelectPDO([]);
      $complement = '';
      
      if($search != null){
        $add = ' WHERE ';
        $params = explode(' ', $search);
        foreach($params as $index => $param){
          $add .= $obj->createLike($param);
          end($params);
          $lastItem = key($params);
          if($lastItem != $index){
            $add .= ' AND ';
          }
        }
        
        // var_dump($add);
        $complement .= $add;
      }
      
      $pre_sql = 'SELECT count(' . $obj->uniqueIdentifierName . ') as num from ' . $obj->getTableName($class) . $complement;
      $pre_sentence = $obj->db->prepare($pre_sql);

      $pre_sentence->execute();

      $pre_sentence->setFetchMode(PDO::FETCH_ASSOC);
      
      $response['length'] = $pre_sentence->fetch()['num'];
      $response['numberOfPages'] = ceil($response['length'] / $response['lines']);
      
      if($orderBy != null){
        $list = $obj->getChildProperties();
        
        if(array_key_exists($orderBy, $list)){
          $complement .= ' ORDER BY ' . $orderBy;
        }
        
      }else{
        $complement .= ' ORDER BY ' . $obj->uniqueIdentifierName;
      }
      
      if(isset($order)){
        $complement .= ' ' . (($order != 'ASC' && $order != 'asc')?'DESC':'ASC');
      }
      
      $complement .= ' limit ' . ($response['init']-1) . ', ' . $length . ';';
      
      // var_dump($complement);
      
      $sql .= $complement;
      
      $sentence = $obj->db->prepare($sql);
      
      // var_dump($sql);

      $sentence->execute();

      $sentence->setFetchMode(PDO::FETCH_CLASS , $class);
      $response['items'] = $sentence->fetchAll();
      
      // var_dump($response);
      
      return $response;
    }
    
    private static function createLike($val){
      $resul = '(';
      $class = get_called_class();
      $obj = new $class();
      $list = $obj->getChildProperties();
        
        foreach($list as $index => $param){
          if($index != $obj->uniqueIdentifierName){
            $resul .= ' ' . $index . ' LIKE \'%' . $val . '%\'';
            end($list);
            $lastItem = key($list);
            if($lastItem != $index){
              $resul .= ' OR';
            }
          }
        }
        $resul .= ')';
        return $resul;
    }
    
    public static function length(){
      $class = get_called_class();
      $obj = new $class();
      $id = $obj->uniqueIdentifierName;
      $sql = "SELECT count(" . $id . ") as length FROM " . $obj->getTableName($class) . ";";
      
      $sentence = $obj->db->prepare($sql);

      $sentence->execute();
      
      $sentence->setFetchMode(PDO::FETCH_ASSOC);
      
      $resul = $sentence->fetch();
      
      return $resul['length'];
    }
    
    private function getTableName($class){
      $list = preg_split("/(?=[A-Z])/", $class);
      
      $list = array_filter($list, function($element){
        return trim($element) != '';
      });
      
      $result = implode($list, "_");
      $result = strtolower($result);
      
      // var_dump($result);
      return $result;
    }
    
    /*
    * @param String $objectName the name of the class
    * @param String $objectReferenceName the name property used as foreing key
    */
    public function hasMany($objectName, $objectReferenceName = null){
      $this->includeClass($objectName);
      $id = $this->uniqueIdentifierName;
      
      if($objectReferenceName == null){
        $class = get_called_class();
        $objectReferenceName = $this->getTableName($class) . '_' . $id;
      }
      
      $response = $objectName::find($this->$id, $objectReferenceName, true);
      
      // var_dump($id);
      // var_dump($objectReferenceName);
      // var_dump($response);
      
      return $response;
    }
    
    public function beyongsTo(){
      
    }
    
    public function beyongsToMany($objectName, $intermedialObjectName, $ownReferenceName = null, $objectReferenceName = null){
      $this->includeClass($objectName);
      $this->includeClass($intermedialObjectName);
      
      $intermedialTable = $this->getTableName($intermedialObjectName);
      $endTable = $this->getTableName($objectName);
      
      if($ownReferenceName == null){
        $id = $this->uniqueIdentifierName;
        $class = get_called_class();
        $ownReferenceName = $this->getTableName($class) . '_' . $id;
      }
      
      if($objectReferenceName == null){
        $obj = new $objectName();
        $id = $obj->uniqueIdentifierName;
        $objectReferenceName = $this->getTableName($objectName) . '_' . $id;
      }
      
      $id = $this->uniqueIdentifierName;
      $intermedialResponse = $intermedialObjectName::find($this->$id, $ownReferenceName, true);
      
      // var_dump($intermedialResponse);
      $result = [];
      
      $obj = new $objectName();
      $id = $obj->uniqueIdentifierName;
      foreach($intermedialResponse as $item){
        $temp = $objectName::find($item->$objectReferenceName, $id);
        
        // var_dump($objectName);
        // var_dump($item->$objectReferenceName);
        // var_dump($id);
        array_push($result, $temp);
      }
      
      
      return $result;
    }
    
    private function includeClass($name){
      require_once $name . '.php';
    }
  }