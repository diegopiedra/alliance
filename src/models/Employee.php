<?php
require_once 'Model.php';
class Employee extends Model{
    protected $id;
    protected $identification;
    protected $name;
    protected $lastname;
    protected $pay_type_id;
    protected $position_id;
    protected $working_day_id;
    protected $phone;
    protected $start_day;
    protected $position_name;
    protected $hours_per_week;
    
    //Agregue los atributos reales del empleado MJ
    public function __construct(){
        parent::__construct();
    }
    
    public function deductions(){
        return $this->beyongsToMany('Deduction', 'EmployeeDeduction');
    }
    
    public function proofsOfPayments(){
        return $this->hasMany('ProofOfPayment');
    }
}