<?php
require_once 'Model.php';

class PayRoll extends Model{
    
    protected $id;
    protected $ordinary_salary;
    protected $extra_hour;
    protected $double_hour;
    protected $position_id;
    protected $pay_type_id;
    protected $working_day_id;
    
    public function __construct(){
        parent::__construct();
    } 
}
