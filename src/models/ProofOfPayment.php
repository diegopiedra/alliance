<?php
require_once 'Model.php';

class ProofOfPayment extends Model{
    
    protected $id;
    protected $responsable_user_id;
    protected $employee_id;
    protected $beagin_of_period;
    protected $end_of_period;
    protected $payday;
    protected $ordinary_salary;
    protected $hours_per_week;
    protected $pay_type;
    protected $position;
    protected $working_day;
    protected $gross_salary;
    protected $net_salary;
    protected $note;
    
    public function __construct(){
        parent::__construct();
    } 
    
    public function increments(){
        return $this->hasMany('Increments');
    }
    
    public function historicalDeductions(){
        return $this->hasMany('HistoricalDeduction');
    }
}
