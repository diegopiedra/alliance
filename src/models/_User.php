<?php
  require_once 'Model.php';
  /**
   *
   */
  class User extends Model
  {
    public $id_user;
    public $name_user;
    public $last_name_user;
    public $email_user;

    function __construct (){
      parent::__construct();
    }

    public function load(){
      $db = $this->getConnection();
      try {
        $query = "SELECT id_user, name_user, last_name_user, email_user FROM User WHERE id_user = :id_user";

        // PDO
        $sentence = $db->prepare($query);
        $sentence->bindParam(':id_user', $this->id_user);
        $sentence->execute();
        $sentence->setFetchMode(PDO::FETCH_CLASS, 'User');

        $user = $sentence->fetch();
        $this->name_user = $user->name_user;
        $this->last_name_user = $user->last_name_user;
        $this->email_user = $user->email_user;

      } catch(PDOException $ex) {
        echo "An Error occured!"; //user friendly message
        some_logging_function($ex->getMessage());
      }
    }
  }

?>
