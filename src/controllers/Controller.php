<?php
    global $base;
    include $base . 'templating/Core/CoreTemplating.php';
    class Controller{
        private $templating;
        public function __construct(){
          $this->templating = new CoreTemplating();
        }

        public function view($view, $params = []){
          $this->templating->render($view, $params);
        }

        public function error404($_vars){
          http_response_code(404);
          $this->view($_vars->view);
        }
        
        public function index($_vars){
          echo 'Function not implement';
        }
        
        public function create($_vars){
          echo 'Function not implement';
        }
        
        public function store($_vars){
          echo 'Function not implement';
        }
        
        public function show($_vars){
          echo 'Function not implement';
        }
        
        public function edit($_vars){
          echo 'Function not implement';
        }
        
        public function update($_vars){
          echo 'Function not implement';
        }
        
        public function destroy($_vars){
          echo 'Function not implement';
        }
    }
?>
