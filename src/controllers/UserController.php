<?php
    require 'Controller.php';
    /*
    *  se importa el modelo del usuario
    */
    require $_ENV['base'] . '/src/models/User.php';
    require_once $_ENV['base'] . '/src/models/Roll.php';

    /*
    *
    */
    class UserController extends Controller
    {
        public function index() {
            if(isset($_SESSION['user_id'])) {
                $users = User::all();
                
                $table = [];
                
                foreach($users as $user) {
                    array_push($table, [
                        'Nombre' => [
                            'data' => $user->name
                        ],
                        'Rol' => [
                            'data' => Roll::find($user->roll_id)->name
                        ],
                        'obj' => $user,
                        'options' => [
                            'show' => '/user/' . $user->id,
                            'delete' => '/user/' . $user->id . '/delete',
                        ]
                    ]);
                }
                
                $this->view('index_users', ['data' => $table]);
            }
            else {
                header('Location: /login');
            }
        }
        
        public function createUser($_vars) {
            if(isset($_SESSION['user_id'])) {
                $rolls = Roll::all();
                
                $this->view('createNewUser', ['rolls' => $rolls]);
            }
            else {
                header('Location: /login');
            }
        }
        
        public function storeUser($_vars) {
            if(isset($_SESSION['user_id'])) {
                $password = $_POST['password'];
                $password_confirm = $_POST['confirm-pass'];
                $name = $_POST['name'];
                $rol_id = $_POST['rol'];
                
                $new_user = new User();
                $new_user->name = $name;
                $new_user->password = password_hash($password, PASSWORD_BCRYPT);
                $new_user->roll_id = $rol_id;
                
                $new_user->save();
                
                header('Location: /user/index');
            }
            else {
                header('Location: /login');
            }
        }
        
        public function showUser($_vars){
            if(isset($_SESSION['user_id'])) {
                $findUser = User::find($_vars->id);
                
                if(is_null($findUser)) {
                    $this->view('index_users?error=No se encuentra el usuario');
                }
                else {
                    $this->view('view_user', ['user' => $findUser]);
                }
            }
            else {
                header('Location: /login');
            }
        }
        
        public function deleteUser($_vars) {
            if(isset($_SESSION['user_id'])) {
                $user = User::find($_vars->id);
                
                if(is_null($user)) {
                    $this->view('index_users?error=Usuario no encontrado');
                }
                else {
                    //Borrado logico
                    $user->delete();
                    header('Location: /user/index?msg=Se ha eliminado el usuario');
                }
            }
            else {
                header('Location: /login');
            }
        }
    }