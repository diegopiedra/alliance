<?php
require_once 'Controller.php';
require_once $_ENV['base'] . '/src/models/PayType.php';
require_once $_ENV['base'] . '/src/models/Position.php';
require_once $_ENV['base'] . '/src/models/WorkingDay.php';
class PayRollController extends Controller{
    public function index($_vars){
        $params = [];
        $params['pay_types'] = PayType::all();
        $params['positions'] = Position::all();
        $params['working_days'] = WorkingDay::all();
        $this->view('pay-roll', $params);
    }
}