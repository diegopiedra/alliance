<?php
    require 'Controller.php';
    require $_ENV['base'] . '/src/models/User.php';
    class SessionController extends Controller
    {
        public function loginScreen($_vars){
          if(isset($_SESSION['user_id'])){
            header('Location: /principal');
          }
          $error = '';
          $this->view('login');
        }
        
        public function validate($_vars){
          $u = $_POST['user'];
          $p = $_POST['password'];
          
          $user = new User();
          $user->name = $u;
          if($user->load() && password_verify($p, $user->password)){
            $_SESSION["user_id"] = $user->id;
            $_SESSION["user_name"] = $user->name;
            header('Location: /principal');
          }else{
            /* JAVIER ESTUVO AQUI XD
                            
            Aqui le hice este cambio, pero solo por si te parece asi jajaja, lo unico es que cuando el
            validate se hace, no redirige a la ruta 'login', si no que muestra 'login/validate' en la
            URL y muestra el error bien sin problemas.
            */
            $this->view('login', ['error'=>'Autenticación fallida, nombre de usuario o contraseña incorrecta']);
            //header('Location: /login?error=Autenticación fallida, nombre de usuario o contraseña incorrecta');
          }
        }
        
        public function ObtenerRol($_vars){
            
             
        }
        
        public function obtenerpermisos($_vars){
          
        }
        
        public function logout($_vars){
          session_unset();
          session_destroy();
          header('Location: /login');
        }
        
        public function getUser(){
          if($_SESSION['user_id']){
            return User::find($_SESSION['user_id']);
          }else{
            return null;
          }
        }
    }
?>
