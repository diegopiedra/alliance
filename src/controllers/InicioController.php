<?php
  require 'Controller.php';
  require $_ENV['base'] . '/src/models/Employee.php';
  
  class InicioController extends Controller
  {
    public function index($_vars){
      header('Location: /login');
      // $this->view('index');
    }

    public function store($_vars)
    {
      echo "Store";
    }

    public function update($_vars)
    {
      echo "Update";
    }

    public function destroy($_vars)
    {
      echo "Destroy";
    }

    public function example($_vars)
    {
      echo "Funcion de ejemplo";
    }

    public function ex2($_vars)
    {
      var_dump($_vars);
    }

    public function user($_vars)
    {
      echo "Usuario: " . $_vars->name;
      if(isset($_REQUEST['age'])){
        echo "<br> Edad: " . $_REQUEST['age'];
      }
    }

    public function template($_vars){
      $name = ucfirst($_vars->name);

      $u1 = new class{};
      $u1->name = 'Diego';
      $u1->lastname = 'Piedra Araya';
      $u1->age = 22;
      $u1->id = 1;

      $u2 = new class{};
      $u2->name = 'Mario';
      $u2->lastname = 'Casa del Bosque';
      $u2->age = 25;
      $u2->id = 2;

      $u3 = new class{};
      $u3->name = 'Dania';
      $u3->lastname = 'Piedra Araya';
      $u3->age = 20;
      $u3->id = 3;

      $u4 = new class{};
      $u4->name = 'Juan';
      $u4->lastname = 'Rojas';
      $u4->age = 20;
      $u4->id = 4;

      $u5 = new class{};
      $u5->name = 'Hector';
      $u5->lastname = 'Vega Fallas';
      $u5->age = 20;
      $u5->id = 5;

      $users = [
        $u1,
        $u2,
        $u3,
        $u4,
        $u5
      ];
      $this->view('basic', [
        'name' => $name, 
        'relation' => 'amigos', 
        'allowed' => $_vars->all, 
        'users' => $users
      ]);
    }

    public function layout($_vars){
      $user = new class{};
      $user->name = 'Mario';
      $user->lastname = 'Casa del Bosque';
      $user->age = 25;
      $user->id = 2;

      $render = ['logued' => true, 'name' => 'Diego', 'user' => $user];
      if(isset($_vars->dato)){
        $render['dato']  = $_vars->dato;
      }
      $this->view('child', $render);
    }

    public function layoutB($_vars){
      $this->view('min');
    }
    
    public function routes(){
      $this->view('routes');
    }
    
    public function css($_vars)
    {
      $app = 'Route base';
      $u1 = new class{};
      $u1->name = 'Diego';
      $u1->lastname = 'Piedra Araya';
      $u1->age = 22;
      $u1->id = 1;

      $u2 = new class{};
      $u2->name = 'Mario';
      $u2->lastname = 'Casa del Bosque';
      $u2->age = 25;
      $u2->id = 2;

      $u3 = new class{};
      $u3->name = 'Dania';
      $u3->lastname = 'Piedra Araya';
      $u3->age = 20;
      $u3->id = 3;

      $u4 = new class{};
      $u4->name = 'Juan';
      $u4->lastname = 'Rojas';
      $u4->age = 20;
      $u4->id = 4;

      $u5 = new class{};
      $u5->name = 'Hector';
      $u5->lastname = 'Vega Fallas';
      $u5->age = 20;
      $u5->id = 5;

      $u6 = new class{};
      $u6->name = 'Victor';
      $u6->lastname = 'Vega Fallas';
      $u6->age = 35;
      $u6->id = 6;

      $u7 = new class{};
      $u7->name = 'Humberto';
      $u7->lastname = 'Vega Fallas';
      $u7->age = 41;
      $u7->id = 7;

      $u8 = new class{};
      $u8->name = 'Randall';
      $u8->lastname = 'Vega Fallas';
      $u8->age = 25;
      $u8->id = 8;

      $users = [
        $u1,
        $u2,
        $u3,
        $u4,
        $u5,
        $u6,
        $u7,
        $u8,
      ];
      $this->view('css', ['users' => $users, 'app' => $app]);
    }
    
    public function data($_vars){
      // $cont = new class{};
      // $cont->data = [
      //   [
      //     'Nombre' => 'Diego',
      //     'Apellidos' => 'Piedra Araya',
      //     'Edad' => 22,
      //     'id' => 1
      //   ],
      //   [
      //     'Nombre' => 'Dania',
      //     'Apellidos' => 'Piedra Araya',
      //     'Edad' => 20,
      //     'id' => 2
      //   ],
      //   [
      //     'Nombre' => 'Juan',
      //     'Apellidos' => 'Rojas Rojas',
      //     'Edad' => 25,
      //     'id' => 4
      //   ],
      //   [
      //     'Nombre' => 'Mario',
      //     'Apellidos' => 'Cazas del Bosque',
      //     'Edad' => 22,
      //     'id' => 4
      //   ]
      // ];
      $employees = Employee::all();
      $employees = array_map(function($item){
        return json_decode($item->__toString());
      }, $employees);
      $this->view('data', [
        'data' =>  $employees
      ]);
    }
    
    public function ejemploDeRelaciones($_vars){
      $employee = Employee::find(2);
      // $employee->hasMany('ProofOfPayment');
      // $d = $employee->beyongsToMany('Deduction', 'EmployeeDeduction');
      $d = $employee->deductions();
      var_dump($d);
    }
  }
?>