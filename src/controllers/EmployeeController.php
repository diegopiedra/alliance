<?php
    require_once 'Controller.php';
    require_once $_ENV['base'] . '/src/models/Employee.php';
    require_once $_ENV['base'] . '/src/models/PayType.php';
    require_once $_ENV['base'] . '/src/models/Position.php';
    require_once $_ENV['base'] . '/src/models/WorkingDay.php';
    require_once $_ENV['base'] . '/src/models/Deduction.php';
    require_once $_ENV['base'] . '/src/models/EmployeeDeduction.php';
    class EmployeeController extends Controller{
        
        public function vistaCrear($_vars){
            if(isset($_SESSION['user_id'])){
                $payTypes = PayType::all();
                $positions = Position::all();
                $workingDays = WorkingDay::all();
                $deductions = Deduction::all();
                
                $this->view('employee', [
                    'payTypes' => $payTypes,
                    'positions' => $positions,
                    'workingDays' => $workingDays,
                    'deductions' => $deductions,
                ]);
            }else{
              header('Location: /login');
            }
        }
        
        public function crear($_vars){
            // $empleado = new Employee();
            // $empleado->identificacion = '123456';
            // $empleado->nombre = 'Maria Jose';
            // $empleado->apellidos= 'Aguirre'; 
            // $empleado->formaDepago = '';
            // $empleado->puesto = 'maestro';
            // $empleado->jornada = '';
            // $empleado->telefono= '';
            // $empleado->puesto = '';
            // $empleado->save();
            // //Agregue los atributos reales del empleado MJ
            // echo $empleado;
        }
        public function mostrar($_vars){
            if(isset($_SESSION['user_id'])){
                $employee = new Employee();
                $employee->id = $_vars->ID;
                $employee->load();
                
                $payTypes = PayType::all();
                $positions = Position::all();
                $workingDays = WorkingDay::all();
                $deductions = Deduction::all();
                
                $employeeDeductions = $employee->deductions();
                $proofsOfPayments = $employee->proofsOfPayments();
                
                // var_dump($proofsOfPayments);
                
                $table = [];
                    
                foreach($proofsOfPayments as $pop){
                    array_push($table, [
                        '#' => [
                            'data' => $pop->id,
                        ],
                        'Fecha de pago' => [
                            'data' => $pop->payday,
                        ],
                        'Inicio del periodo' => [
                            'data' => $pop->beagin_of_period,
                        ],
                        'Fin del periodo' => [
                            'data' => $pop->end_of_period,
                        ],
                        'Salario bruto' => [
                            'data' => '₡ ' . money_format('%i', $pop->gross_salary),
                        ],
                        'Salario neto' => [
                            'data' => '₡ ' . money_format('%i', $pop->net_salary),
                        ],
                        'obj' => $pop,
                        'options' => [
                            'show' => '/comprobante-de-pago/' . $pop->id,
                            'delete' => '/comprobante-de-pago/' . $pop->id . '/borrar',
                        ]
                    ]);
                }
                
                $this->view('employee', [
                    'employee' => $employee,
                    'payTypes' => $payTypes,
                    'positions' => $positions,
                    'workingDays' => $workingDays,
                    'deductions' => $deductions,
                    'employeeDeductions' => $employeeDeductions,
                    'data' => $table
                ]);
            }else{
              header('Location: /login');
            }
        }
       
        public function index($_vars){
            if(isset($_SESSION['user_id'])){
                
                $page = (isset($_GET['page']))?$_GET['page']:1;
                $pageLength = (isset($_GET['page_length']) && $_GET['page_length'] > 0)?$_GET['page_length']:10;
                $order = (isset($_GET['order']))?$_GET['order']:'ASC';
                
                $orderBy = (isset($_GET['order_by']) && $_GET['order_by'] != null && $_GET['order_by'] != 'default')?$_GET['order_by']:null;
                
                $search = (isset($_GET['search']) && $_GET['search'] != '')?$_GET['search']:null;
                
                $employessPaginate = Employee::paginate($page,$pageLength, $order, $orderBy, $search); 
                
                
                
                $employess = $employessPaginate ['items'];
                
                $length = $employessPaginate['length'];
                
                $table = [];
                
                foreach($employess as $employee){
                    array_push($table, [
                        'Identificación' => [
                            'data' => $employee->identification,
                            'bind' => 'identification'
                        ],
                        'Nombre' => [
                            'data' => $employee->name,
                            'bind' => 'name'
                        ],
                        'Apellidos' => [
                            'data' => $employee->lastname,
                            'bind' => 'lastname'
                        ],
                        'Teléfono' => [
                            'data' => $employee->phone,
                            'bind' => 'phone'
                        ],
                        'obj' => $employee,
                        'options' => [
                            'show' => '/empleado/' . $employee->id,
                            'delete' => '/empleado/' . $employee->id . '/borrar',
                        ]
                    ]);
                }
                
                
                $this->view('employees', ['data' => $table, 'length' => $length, 'paginate' => $employessPaginate]);
              
            }else{
              header('Location: /login');
            }
            
        }
        
        public function store($_vars){
            if(isset($_POST['id']) && $_POST['id'] != 0 && trim($_POST['id']) != ''){
                $this->updateEmployee();
            }else{
                $this->createNewEmployee();
            }
        }
        
        private function updateEmployee(){
            $employee = Employee::find($_POST['id']);
            if($employee != null){
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }else{
                
            }
        }
            
        private function createNewEmployee(){
            $employee = new Employee();
            
            $employee->identification = $_POST['identification'];
            
            if($employee->load()){
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }else{
                $employee->identification = $_POST['identification'];
                $employee->name = $_POST['name'];
                $employee->lastname = $_POST['lastname'];
                $employee->pay_type_id = $_POST['pay_type_id'];
                $employee->position_id = $_POST['position_id'];
                $employee->working_day_id = $_POST['working_day_id'];
                $employee->phone = $_POST['phone'];
                $employee->start_day = $_POST['start_day'];
                
                if(trim($_POST['position_name']) != ''){
                    $employee->position_name = $_POST['position_name'];
                }
                
                if(trim($_POST['hours_per_week']) != ''){
                    $employee->hours_per_week = $_POST['hours_per_week'];
                }
                
                $employee->save();
                foreach($_POST['deductions'] as $de){
                    $deduction = new EmployeeDeduction();
                    $deduction->employee_id = $employee->id;
                    $deduction->deduction_id = $de;
                    $deduction->save();
                }
                header('Location: /empleado/' . $employee->id);
            }
        }
    }