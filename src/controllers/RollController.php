<?php
    require_once 'Controller.php';
    require_once $_ENV['base'] . '/src/models/Roll.php';
    
    class RollController extends Controller {
        public function index() {
            if(isset($_SESSION['user_id'])) {
                $rolls = Roll::all();
                $table = [];
                
                foreach($rolls as $role) {
                    array_push($table, [
                        'ID' => [
                            'data' => $role->id,
                            'bind' => 'id'
                        ],
                        'Nombre' => [
                            'data' => $role->name,
                            'bind' => 'name'
                        ],
                        'obj' => $role,
                        'options' => [
                            'show' => '/roles/' . $role->id,
                            'delete' => '/roles/delete/' . $role->id
                        ]
                    ]);
                }
                
                $this->view('index_rolls', ['data' => $table]);
            }
            else {
                header('Location: /login');
            }
        }
        
        public function create() {
            if(isset($_SESSION['user_id'])) {
                $this->view('create_rol');
            }
            else {
                header('Location: /login');
            }
        }
        
        public function store() {
            if(isset($_SESSION['user_id'])) {
                $role_name = $_POST['name'];
                
                $role = new Roll();
                $role->name = $role_name;
                
                $role->save();
                
                header('Location: /roles/index');
            }
            else {
                header('Location: /login');
            }
        }
        
        public function viewRoll($_vars) {
            if(isset($_SESSION['user_id'])) {
                $roll = new Roll();
                $roll->id = $_vars->id;
                $roll->load();
                
                $this->view('view_rol', ['role' => $roll]);
            }
            else {
                header('Location: /login');
            }
        }
        
        public function deleteRoll($_vars) {
            if(isset($_SESSION['user_id'])) {
                $role = Roll::find($_vars->id);
                
                if(is_null($user)) {
                    $this->view('index_rolls?error=Rol no encontrado');
                }
                else {
                    //Borrado logico
                    $role->delete();
                    header('Location: /roles/index?msg=Se ha eliminado el rol');
                }
            }
            else {
                header('Location: /login');
            }
        }
    }