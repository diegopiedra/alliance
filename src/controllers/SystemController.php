<?php
  require 'Controller.php';
  require $_ENV['base'] . '/src/models/User.php';
  class SystemController extends Controller
  {
    public function index($_vars){
        if(isset($_SESSION['user_id'])){
          $this->view('inicio', [
            'session_user' => User::find($_SESSION['user_id'])
          ]);
        }else{
          header('Location: /login');
        }
    }
  }
  