<?php
require_once 'Controller.php';
require_once $_ENV['base'] . '/src/models/ProofOfPayment.php';
require_once $_ENV['base'] . '/src/models/Employee.php';
require_once $_ENV['base'] . '/src/models/User.php';
require_once $_ENV['base'] . '/src/models/Position.php';
require_once $_ENV['base'] . '/src/models/PayType.php';
require_once $_ENV['base'] . '/src/models/WorkingDay.php';
require_once $_ENV['base'] . '/src/models/PayRoll.php';
require_once $_ENV['base'] . '/src/models/Increments.php';
require_once $_ENV['base'] . '/src/models/HistoricalDeduction.php';
class ProofOfPaymentController extends Controller
{
    public function show($_vars){
      if(isset($_SESSION['user_id'])){
        
        $params = [];
        
        $proof = $params['proof'] = ProofOfPayment::find($_vars->id);
        
        // var_dump($params['proof']->id);
        
        if(isset($proof) && $proof != null && $proof != ''){
          $proof->payday = substr($proof->payday, 0, 10);
          $params['employee'] = Employee::find($proof->employee_id);
          $params['increments'] = $proof->increments();
          $params['deductions'] = $proof->historicalDeductions();
          $params['user'] = User::find($proof->responsable_user_id);
          $params['final'] = true;
          $params['increments_total'] = 0;
          $params['deductions_total'] = 0;
          
          foreach($params['increments'] as $i){
            $params['increments_total'] += doubleval($i->value);
          }
          
          foreach($params['deductions'] as $d){
            $params['deductions_total'] += doubleval($d->total);
          }
        }
        
        $this->view('proof-of-payment', $params);
      }else{
        header('Location: /login');
      }
      
    }
    
    public function index($_vars){
      if(isset($_SESSION['user_id'])){
        
        $page = (isset($_GET['page']))?$_GET['page']:1;
        $pageLength = (isset($_GET['page_length']) && $_GET['page_length'] > 0)?$_GET['page_length']:10;
        $order = (isset($_GET['order']))?$_GET['order']:'ASC';
        
        $orderBy = (isset($_GET['order_by']) && $_GET['order_by'] != null && $_GET['order_by'] != 'default')?$_GET['order_by']:null;
        
        $search = (isset($_GET['search']) && $_GET['search'] != '')?$_GET['search']:null;
        
        $proofOfPaymentPaginate = ProofOfPayment::paginate($page,$pageLength, $order, $orderBy, $search);
        
        
        
        
        
        $proofOfPayments = $proofOfPaymentPaginate['items'];
                
        $length = $proofOfPaymentPaginate['length'];
        
        $table = [];
        
        foreach($proofOfPayments as $pop){
            $employee = Employee::find($pop->employee_id);
            
            array_push($table, [
              '#' => [
                  'data' => $pop->id,
                  'bind' => 'id',
              ],
              'Fecha de pago' => [
                  'data' => $pop->payday,
                  'bind' => 'payday',
              ],
              'Periodo' => [
                  'data' => $pop->beagin_of_period . ' - ' . $pop->end_of_period
              ],
              'Empleado' => [
                  'data' => $employee->name . ' ' . $employee->lastname
              ],
              // 'Salario bruto' => [
              //     'data' => '₡ ' . money_format('%i', $pop->ordinary_salary),
              //     'bind' => 'ordinary_salary',
              // ],
              // 'Salario neto' => [
              //     'data' => '₡ ' . money_format('%i', $pop->final_salary),
              //     'bind' => 'final_salary',
              // ],
              'obj' => $pop,
              'options' => [
                  'show' => '/comprobante-de-pago/' . $pop->id,
                  'delete' => '/comprobante-de-pago/' . $pop->id . '/borrar',
              ]
          ]);
        }
        
        // $this->view('employees', ['data' => $table, 'length' => $length, 'paginate' => $employessPaginate]);
        
        $this->view('proofs-of-payments', ['data' => $table, 'length' => $length, 'paginate' => $proofOfPaymentPaginate]);
      }else{
        header('Location: /login');
      }
    }
    
    public function create($_vars){
      if(isset($_SESSION['user_id'])){
        if(isset($_SESSION['employee_for_proof_of_payment_id'])){
          $id = $_SESSION['employee_for_proof_of_payment_id'];
          unset($_SESSION['employee_for_proof_of_payment_id']);
          
          $employee = Employee::find($id);
          
          $params = [];
          $params['employee'] = $employee;
          $params['increments_total'] = 0;
          
          if($employee->position_name != null && trim($employee->position_name) != ''){
            $position = $employee->position_name;
          }else{
            $p = Position::find($employee->position_id);
            $position = $p->name;
          }
          
          
          $payType = PayType::find($employee->pay_type_id);
          $workingDay = WorkingDay::find($employee->working_day_id);
          
          $payRoll = new PayRoll();
          $payRoll->position_id = $employee->position_id;
          $payRoll->pay_type_id = $employee->pay_type_id;
          $payRoll->working_day_id = $employee->working_day_id;
          $payRoll->load();
          
          $proof = new ProofOfPayment();
          $proof->position = $position;
          $proof->pay_type = $payType->name;
          $proof->working_day = $workingDay->name;
          $proof->payday = date("Y-m-d"); //date("Y-m-d H:i:s");
          $proof->beagin_of_period = date("Y-m-d"); //date("Y-m-d H:i:s");
          $proof->end_of_period = date("Y-m-d"); //date("Y-m-d H:i:s");
          $proof->ordinary_salary = $payRoll->ordinary_salary;
          $proof->gross_salary = $payRoll->ordinary_salary;
          
          $deductions = $employee->deductions();
          
          $params['deductions'] = [];
          $singleDeduction = (count($deductions) == 1);
          $deductionsTotal = 0;
          foreach($deductions as $deduction){
            $obj = new class{};
            $obj->name = $deduction->name;
            $obj->percentaje = ($singleDeduction)?$deduction->single_percentage:$deduction->shared_percentage;
            
            $obj->total = $proof->gross_salary * ($obj->percentaje / 100);
            
            $deductionsTotal += $obj->total;
            array_push($params['deductions'], $obj);
          }
          
          $params['increments'] = [];
          
          $proof->net_salary = $proof->gross_salary - $deductionsTotal;
          
          $params['deductions_total'] = $deductionsTotal;
          $params['proof'] = $proof;
          
          $this->view('proof-of-payment', $params);
        }else{
          $this->view('proof-of-payment-req-employee');
        }
      }else{
        header('Location: /login');
      }
    }
    
    public function employeeSelect($_vars){
      
      if(isset($_SESSION['user_id'])){
        unset($_SESSION['employee_for_proof_of_payment_id']);
        
        $employee = Employee::find($_POST['identification'], 'identification');
        if(is_null($employee)){
          header('Location: /comprobante-de-pago/nuevo?error=La identificación no coincide con ningún empleado');
        }else{
          $_SESSION['employee_for_proof_of_payment_id'] = $employee->id;
          header('Location: /comprobante-de-pago/nuevo');
        }
      }else{
        header('Location: /login');
      }
    }
    
    public function store($_var){
      if(isset($_SESSION['user_id'])){
        
        $proof = new ProofOfPayment();
        $proof->responsable_user_id = $_SESSION['user_id'];
        $proof->employee_id = $_POST['employee_id'];
        $proof->beagin_of_period = $_POST['beagin_of_period'];
        $proof->end_of_period = $_POST['end_of_period'];
        $proof->payday = $_POST['payday'];
        $proof->ordinary_salary = $_POST['ordinary_salary'];
        $proof->pay_type = $_POST['pay_type'];
        $proof->position = $_POST['position'];
        $proof->working_day = $_POST['working_day'];
        
        if(trim($_POST['note']) != ''){
          $proof->note = $_POST['note'];
        }
        
        if(isset($_POST['hours_per_week'])){
          $proof->hours_per_week = $_POST['hours_per_week'];
        }
        
        $totalIncrements = 0;
        foreach($_POST['increments'] as $inc){
          $val = $inc['value'];
          if($val > 0){
            $totalIncrements += $val;
          }
          
        }
        $proof->gross_salary = $proof->ordinary_salary + $totalIncrements;
        
        $totalDeductions = 0;
        $totalPercentage = 0;
        foreach($_POST['deductions'] as $deduction){
          $per = $deduction['percentaje'];
          if($per > 0 && ($per + $totalPercentage) <= 100){
            $totalPercentage += $per;
            $totalDeductions += $proof->gross_salary * ($per/100);
          }
        }
        $proof->net_salary = $proof->gross_salary - $totalDeductions;
        
        $proof->save();
        
        foreach($_POST['increments'] as $inc){
          $val = $inc['value'];
          if($val != null && $val != '' && $val > 0){
            $increment = new Increments();
            $increment->proof_of_payment_id = $proof->id;
            $increment->quantity = $inc['quantity'];
            $increment->name = $inc['name'];
            $increment->value = $inc['value'];
            
            $increment->save();
          }
        }
        
        
        $totalPercentage = 0;
        foreach($_POST['deductions'] as $ded){
          $per = $ded['percentaje'];
          if($per != null && $per != '' && $per > 0 && ($per + $totalPercentage) <= 100){
            $deduction = new HistoricalDeduction();
            
            $totalPercentage += $per;
            $total = $proof->gross_salary * ($per/100);
            
            $deduction->proof_of_payment_id = $proof->id;
            $deduction->percentaje = $per;
            $deduction->name = $ded['name'];
            $deduction->total = $total;
            
            $deduction->save();
          }
        }
        header('Location: /comprobante-de-pago/' . $proof->id);
      }else{
        header('Location: /login');
      }
    }
}