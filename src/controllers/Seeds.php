<?php
require_once $_ENV['base'] . '/src/models/Employee.php';
require_once $_ENV['base'] . '/src/models/Roll.php';
require_once $_ENV['base'] . '/src/models/Position.php';
require_once $_ENV['base'] . '/src/models/WorkingDay.php';
require_once $_ENV['base'] . '/src/models/PayType.php';
require_once $_ENV['base'] . '/src/models/PayRoll.php';
require_once $_ENV['base'] . '/src/models/Deduction.php';
require_once $_ENV['base'] . '/src/models/EmployeeDeduction.php';
require_once $_ENV['base'] . '/src/models/User.php';
require_once $_ENV['base'] . '/src/models/ProofOfPayment.php';
require_once $_ENV['base'] . '/src/models/HistoricalDeduction.php';
require_once $_ENV['base'] . '/src/models/Increments.php';

class Seeds{
    
    public function index($_vars){
        $func = $_vars->func;
        $this->$func($_vars);
    }
    
    public function run($_vars){
        $this->rollSeeder($_vars);
        $this->userSeeder($_vars);
        $this->positionSeeder($_vars);
        $this->working_daySeeder($_vars);
        $this->pay_typeSeeder($_vars);
        $this->employeeSeeder($_vars);
        $this->pay_rollSeeder($_vars);
        $this->deductionSeeder($_vars);
        $this->employeeDeductionSeeder($_vars);
        $this->proof_of_paymentSeeder($_vars);
        $this->historical_deductionSeeder($_vars);
        $this->incrementsSeeder($_vars);

        // permissions
        // roll_has_permissions
        // change_log
    }
    
    public function incrementsSeeder($_vars){
        // HistoricalDeduction
        $proofs = ProofOfPayment::all();
        
        foreach($proofs as $proof){
            if(rand(0, 1)){
                $hd = new Increments();
                $hd->proof_of_payment_id = $proof->id;
                $hd->quantity = rand(1,4);
                $hd->name = 'Incremento por una razón';
                $hd->value = rand(500,7000);
                $hd->save();
            }else{
                $hd = new Increments();
                $hd->proof_of_payment_id = $proof->id;
                $hd->quantity = rand(1,4);
                $hd->name = 'Incremento por una razón';
                $hd->value = rand(500,7000);
                $hd->save();
                
                $hd = new Increments();
                $hd->proof_of_payment_id = $proof->id;
                $hd->quantity = rand(1,4);
                $hd->name = 'Incremento por una razón';
                $hd->value = rand(500,7000);
                $hd->save();
            }
        }
    }
    
    public function historical_deductionSeeder($_vars){
        // HistoricalDeduction
        $proofs = ProofOfPayment::all();
        
        foreach($proofs as $proof){
            if(rand(0, 1)){
                $hd = new HistoricalDeduction();
                $hd->proof_of_payment_id = $proof->id;
                $hd->percentaje = rand(2,20);
                $hd->name = 'Deducción por una razón';
                $hd->total = rand(500,7000);
                $hd->save();
            }else{
                $hd = new HistoricalDeduction();
                $hd->proof_of_payment_id = $proof->id;
                $hd->percentaje = rand(2,20);
                $hd->name = 'Deducción por una razón';
                $hd->total = rand(500,7000);
                $hd->save();
                
                $hd = new HistoricalDeduction();
                $hd->proof_of_payment_id = $proof->id;
                $hd->percentaje = rand(2,20);
                $hd->name = 'Deducción por una razón';
                $hd->total = rand(500,7000);
                $hd->save();
            }
        }
    }
    
    public function proof_of_paymentSeeder($_vars){
        $users = User::all();
        $employees = Employee::all();
        $positions = Position::all();
        $pay_types = PayType::all();
        $working_days = WorkingDay::all();
        
        foreach($employees as $employee){
            $proofs = rand(-1,3);
            
            for ($i = 0; $i < $proofs; $i++) {
                $proof = new ProofOfPayment();
                $proof->responsable_user_id = $users[rand(1, (count($users) - 1))]->id;
                $proof->employee_id = $employee->id;
                
                $month = rand(10,12);
                $proof->beagin_of_period = '2018-' . $month . '-01';
                $proof->end_of_period = '2018-' . $month . '-07';
                
                $proof->payday = '2018-' . $month . '-08 09:35:44';
                $proof->ordinary_salary = rand(1500,87000);
                $proof->gross_salary = rand(1500,87000);
                $proof->net_salary = rand(1500,87000);
            
                
                $proof->pay_type = $pay_types[rand(0, count($pay_types) - 1)]->name;
                $proof->position = $positions[rand(0, count($positions) - 1)]->name;
                $proof->working_day = $working_days[rand(0, count($working_days) - 1)]->name;
            
                
                $proof->hours_per_week = rand(5,48);
                
                $proof->note = 'Observaciones: Desglose de horas laboradas: Laboró lunes 09/04/2018, 6 horas diurnas de 7:00 am a 13:00 pm. Laboró martes 10/04/2018, 6 horas diurnas de 7:00 am a 13:00 pm. Laboró miércoles 11/04/2018, 6 horas diurnas de 7:00 am a 13:00 pm. Laboró jueves 12/04/2018, 6 horas diurnas de 7:00 am a 13:00 pm. Laboró viernes 13/04/2018, 6 horas diurnas de 7:00 am a 13:00 pm  Para un total de 30 horas ordinarias diurnas laboradas a la semana. Colaborador disfruta del feriado del 11 de abril. Se paga dicho feriado.';
                $proof->final_salary = rand(19500,175000);
                $proof->save();
            }
        }
    }
    
    public function employeeDeductionSeeder($_vars){
        $employees = Employee::all();
        
        foreach($employees as $employee){
            if(rand(0,1)){
                
                $relation = new EmployeeDeduction();
                $relation->employee_id = $employee->id;
                $relation->deduction_id = rand(1,2);
                $relation->save();
                
            }elseif(rand(0,1)){
                $relation = new EmployeeDeduction();
                $relation->employee_id = $employee->id;
                $relation->deduction_id = 1;
                $relation->save();
                
                $relation = new EmployeeDeduction();
                $relation->employee_id = $employee->id;
                $relation->deduction_id = 2;
                $relation->save();
            }
        }
    }
    
    public function deductionSeeder($_vars){
        $deduction = new Deduction();
        $deduction->name = 'CCSS';
        $deduction->single_percentage = 10.34;
        $deduction->shared_percentage = 6.50;
        $deduction->save();
        
        $deduction = new Deduction();
        $deduction->name = 'JUPEMA';
        $deduction->single_percentage = 15;
        $deduction->shared_percentage = 8.50;
        $deduction->save();
    }
    
    public function pay_rollSeeder($_vars){
        
        $positions = Position::all();
        $pay_types = PayType::all();
        $working_days = WorkingDay::all();
        
        foreach($positions as $position){
            foreach($pay_types as $pay_type){
                foreach($working_days as $working_day){
                    $payRoll = new PayRoll();
                    $payRoll->ordinary_salary = rand(1500, 850000);
                    
                    if(rand(0,1)){
                        $payRoll->extra_hour = $payRoll->ordinary_salary + ($payRoll->ordinary_salary * 0.5);
                    }else{
                        $payRoll->extra_hour = 0;
                    }
                    
                    if(rand(0,1)){
                        $payRoll->double_hour = $payRoll->ordinary_salary + $payRoll->ordinary_salary;
                    }else{
                        $payRoll->double_hour = 0;
                    }
                    
                    $payRoll->position_id = $position->id;
                    $payRoll->pay_type_id = $pay_type->id;
                    $payRoll->working_day_id = $working_day->id;
                    
                    $payRoll->save();
                }
            }
        }
    }
    
    public function pay_typeSeeder($_vars){
        
        $payTypes = [
            'Quincenal',
            'Semanal',
            'Por hora de pago semanal',
        ];
        
        foreach($payTypes as $wd){
            $payType = new PayType();
            $payType->name = $wd;
            $payType->save();
        }
    }
    
    public function working_daySeeder($_vars){
        
        $wday = [
            'Acumulativa 48 horas semanales',
            'Disminuida 45 horas semanales',
            'Disminuida 40 horas semanales',
            'Diurna'
        ];
        
        foreach($wday as $wd){
            $w = new WorkingDay();
            $w->name = $wd;
            $w->save();
            // var_dump($w);
        }
        
        echo '<br> WorkingDay OK';
    }
    
    public function positionSeeder($_vars){
        
        $pos = [
            'Niñera',
            'Misceláneo',
            'Cocinera',
            'Docente',
            'Educador aspirante sin título',
            'Ayudante de cocina',
            'Docente cómputo (Bach)',
            'Docente educación en valores (Bach)',
        ];
        
        foreach($pos as $p){
            $position = new Position();
            $position->name = $p;
            $position->save();
        }
        
        // var_dump(Position::all());
        
        echo '<br> Position OK';
    }
    
    public function userSeeder($_vars){
        $roll = Roll::all()[0];
        $user = new User();
        $user->name = 'admin';
        $user->password = password_hash('1234', PASSWORD_BCRYPT);
        $user->roll_id = $roll->id;
        $user->save();
        
        $user = new User();
        $user->name = 'diego';
        $user->password = password_hash('asd5648D45fwf', PASSWORD_BCRYPT);
        $user->roll_id = $roll->id;
        $user->save();
        
        $user = new User();
        $user->name = 'javi';
        $user->password = password_hash('1234', PASSWORD_BCRYPT);
        $user->roll_id = $roll->id;
        $user->save();
        
        
        echo '<br> User OK';
        
        // var_dump(Roll::all());
    }
    
    public function rollSeeder($_vars){
        $roll = new Roll();
        
        $roll->name = 'admin';
        $roll->save();
        echo '<br> Roll OK';
        // var_dump(Roll::all());
    }
    
    public function employeeSeeder($_vars){
        
        $positions = Position::all();
        $pay_types = PayType::all();
        $working_days = WorkingDay::all();
        
        $name = [
            'Diego',
            'Dania',
            'Daniel',
            'Daniela',
            'Javier',
            'Jenifer',
            'María',
            'José',
            'María José',
            'Stephanie',
            'Jackeline',
            'Andrey',
            'Celina',
            'Fabiana',
            'Quebeth',
            'Yeffry',
            'Lauren',
            'Sharon',
            'Yamileth',
            'Armando',
            'Gilberth',
            'Luis',
            'Melany',
            'Katherine',
            'Vannesa',
            'Maricela',
            'Allison',
            'Brayan',
            'Rebeca',
            'Jeremy',
            'Jeimy',
            'Karen',
            'Alejandra',
            'Wendy',
            'Mariana',
            'Thomas',
            'Eduardo',
            'Walter',
        ];
        
        $lastname = [
            'Piedra',
            'Araya',
            'Arias',
            'Alvarez',
            'Rojas',
            'Valverde',
            'Rodriguez',
            'Cordero',
            'Blanco',
            'Cerdas',
            'Navas',
            'Aguirre',
            'Solis',
            'Cambronero',
            'Davila',
            'Candia',
            'Vega',
            'Dover',
            'Marín',
            'Hernández',
        ];
        
        $employee = new Employee();
        $employee->identification = '207400490';
        $employee->name = 'Diego José';
        $employee->lastname = 'Piedra Araya';
        $employee->pay_type_id = $pay_types[rand(0, count($pay_types) - 1)]->id;
        $employee->position_id = $positions[rand(0, count($positions) - 1)]->id;
        $employee->working_day_id = $working_days[rand(0, count($working_days) - 1)]->id;
        $employee->phone = '86556068';
        $employee->start_day = '2018/06/06 ' . rand(10,12) . ':' . rand(10,59) . ':' . rand(10,59);
        $employee->save();
        
        for ($i = 0; $i < 59; $i++) {
            $employee = new Employee();
            $employee->identification = rand(100000000,999999999);
            $employee->name = $name[rand(0,count($name)-1)];
            $employee->lastname = $lastname[rand(0,count($lastname)-1)] . ' ' . $lastname[rand(0,count($lastname)-1)];
            $employee->pay_type_id = $pay_types[rand(0, (count($pay_types) - 1))]->id;
            $employee->position_id = $positions[rand(0, (count($positions) - 1))]->id;
            $employee->working_day_id = $working_days[rand(0, (count($working_days) - 1))]->id;
            $employee->phone = rand(80000000, 89999999);
            $employee->start_day = '2018/06/06 ' . rand(10,12) . ':' . rand(10,59) . ':' . rand(10,59);
            $employee->save();
        }
        
        // var_dump(Employee::all());
        echo '<br> Employee OK';
    }   
}