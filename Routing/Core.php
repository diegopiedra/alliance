<?php
  require 'autoload.php';
  /**
   * Thos is the main class of the program
   */
  class Core
  {
    public function main($error404, $origin) {
      $route = Router::getCurrentRoute();
      $router = new Router("routes.php", "/{\S[^}{]*}/", $error404);

      $path = $router->getFilePath($route);
      include($origin.$path . '.php');

      $func = $router->getPathFunction($route);

      $controller = $this->clearControllerName($path);
      $instance = new $controller();
      $method = $router->getMethod();

      $_vars = $router->getVars();
      if($func != 'default'){
        if(method_exists($instance, $func)){
          $instance->$func($_vars);
        }else{
          var_dump("The function $func not exist in " . get_class($instance));
        }
      }else{
        switch ($method) {
          case 'GET':
            $instance->index($_vars);
            break;
          case 'POST':
            $instance->store($_vars);
            break;
          case 'PUT':
          case 'PATCH':
            $instance->update($_vars);
            break;
          case 'DELETE':
            $instance->destroy($_vars);
            break;
          }
      }
    }

    private function clearControllerName($path){
      $parts = explode('/', $path);
      return $parts[count($parts)-1];
    }
  }

?>
