<?php
 return [
    '/' => 'InicioController@index',
    '/css' => 'InicioController@css',
    // '/pizza' => 'InicioController@pizza',
    '/routes' => 'InicioController@routes',
    // '/mvc' => 'InicioController@mvc',
    '/user' => 'UserController',
    '/mj' => 'MjController@index',
    '/principal' => 'SystemController@index',
    '/data' => 'InicioController@data',
    '/login' => 'SessionController@loginScreen',
    '/login/validate' => 'SessionController@validate',
    '/logout' => 'SessionController@logout',
    '/crear-comprobante' => 'ProofOfPaymentController@create',
    // '/empleado' => 'EmployeeController@index',
    '/empleado/crear'=> 'EmployeeController@vistaCrear',
    '/empleado/guardar'=> 'EmployeeController@store',
    '/empleado/{ID}'=> 'EmployeeController@mostrar',
    '/empleado'=> 'EmployeeController@crear',
    '/empleado/{ID}/editar'=> 'EmployeeController@editar',
    '/empleados' => 'EmployeeController@index',
    //clave->valor(controlador@metodo)
    '/user/index' => 'UserController@index',
    '/user/createUser' => 'UserController@createUser',
    '/user/store' => 'UserController@storeUser',
    '/user/{id}' => 'UserController@showUser',
    '/user/{id}/delete' => 'UserController@deleteUser',
    
    //roles
    '/roles/index' => 'RollController@index',
    '/roles/create' => 'RollController@create',
    '/roles/store' => 'RollController@store',
    '/roles/{id}' => 'RollController@viewRoll',
    // '/roles/delete/{id}' => 'RollController@deleteRoll',
    
    '/seeders/run' => 'Seeds@run',
    '/seeders/{func}' => 'Seeds@index',
    
    // '/relaciones' => 'InicioController@ejemploDeRelaciones',
    
    '/comprobantes-de-pago' => 'ProofOfPaymentController@index',
    '/comprobante-de-pago/nuevo' => 'ProofOfPaymentController@create',
    '/comprobante-de-pago/selecion-de-empleado' => 'ProofOfPaymentController@employeeSelect',
    '/comprobante-de-pago/guardar' => 'ProofOfPaymentController@store',
    '/comprobante-de-pago/{id}' => 'ProofOfPaymentController@show',
    
    '/planillas' => 'PayRollController@index',
  ];
