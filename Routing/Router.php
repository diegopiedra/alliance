<?php
  /**
   * Class by routing
   */
  class Router
  {
    private $route;
    private $routes;
    private $varRule;
    private $error404;
    private $_vars;

    function __construct($routesFilePath, $varRule, $error404)
    {
      $this->routes = require_once($routesFilePath);
      $this->varRule = $varRule;
      $this->error404 = $error404;
      $this->_vars = new class{};
    }

    /**
    * @return object with properties like variables pass in the URL format
    **/
    public function getVars()
    {
      return $this->_vars;
    }

    /**
    * @return A route path affter page base name
    **/
    public static function getCurrentRoute()
    {
      $route = (array_key_exists('REQUEST_URI', $_SERVER))?$_SERVER['REQUEST_URI']:'';
    //   $less = rtrim($route, "/");
    // /* if($route != $less){
    //     header('Location: '. $less);
    //     $route = $less;
    //   }*/
    //   var_dump($route);
    //   var_dump($less);
      return $route;
    }

    /**
    * @param String $route: The text to compare whit created patterns
    * @return String: The file source path route same as $route, if not exist return null
    **/
    public function getFilePath($route)
    {
      foreach ($this->routes as $name => $value) {
        if($this->match($route, $name)){
          return explode('@', $value)[0];
        }
      }
      $this->_vars = (object) ['view' => $this->error404];
      return 'Controller';
    }

    /**
    * @param String $route: The text to compare whit created patterns
    * @return String: The file source path route same as $route, if not exist return null
    **/
    public function getPathFunction($route)
    {
      foreach ($this->routes as $name => $value) {
        if($this->match($route, $name)){
          return (count(explode('@', $value)) === 2)?explode('@', $value)[1]:'default';
        }
      }
      return 'error404';
    }

    /**
    * @param String $route
    * @param String $pattern
    * @return Boolean: if $route has the same structure as $pattern
    **/
    private function match($route, $pattern)
    {
      $route = explode('?', $route);
      if(count($route) > 1){
        array_pop($route);
      }
      $route = implode('?', $route);
      
      if($route != "" && $pattern == ""){
        return false;
      }

      if($route == $pattern){
        return true;
      }

      $vars = [];
      $parts = $this->getPartsToPattern($pattern);

      foreach ($parts as $part) {
        $init = $this->indexToInit($route, $part);
        if(!is_numeric($init)){
          return false;
        }elseif ($init > 0) {
          array_push($vars, substr($route, 0 , $init));
          $route = substr($route, $init+strlen($part));
        }else{
          $route = substr($route, strlen($part));
        }
      }
      if($route != ""){
        return false;
      }

      $this->setUserVarialbes($vars, $pattern);
      return true;
    }

    private function setUserVarialbes($values, $route)
    {
      $result = [];
      preg_match_all($this->varRule, $route, $names);
      foreach ($names[0] as $index => $name) {
        $name = trim($name, "{}");
        $result[$name] = (count($values)>$index)?$values[$index]:"";
      }
      $this->_vars = (object) $result;
    }

    private function indexToInit($route, $part)
    {
      if($part == "") {
        return strlen($route);
      }else{
        return strpos($route, $part);
      }
    }

    /**
    * @param String $pattern
    * @return String[]
    **/
    public function getPartsToPattern($pattern)
    {
      return preg_split($this->varRule, $pattern);
    }

    /**
    * @return String: Get the http method use in the request
    **/
    public function getMethod()
    {
      return (isset($_REQUEST['_method']))?$_REQUEST['_method']:$_SERVER['REQUEST_METHOD'];
    }
  }
 ?>
